<!DOCTYPE html>
<html>
  <head>
    <title>TEDxKraków - Inspiring people who are up to something since 2010</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>

    <!-- HEADER -->
    <div id="header" class="section header">
      <div class="mobile-header">
        <div class="mobile-content-wrapper">
          <div class="row">
            <a href="#eventsSection" rel="" class="text" key="start"></a>
            <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
          </div>
          <div class="row">
            <a href="#partners" rel="" class="text" key="partners"></a>
            <a href="#newsletter" rel="" class="text" key="newsletter"></a>
          </div>
          <div class="row">
            <a href="https://medium.com/tedxkrakow" target="_blank" rel="" class="text" key="blog"></a>
            <a href="#contact" rel="" class="text" key="contact"></a>
          </div>
          <div class="row">
            <span class="language-select">
              <a onclick="changeLng('pl')" rel="" id="language-pl-selector" class="text" key="pl">Wersja polska</a>
            </span>
            <span class="language-select">
              <a onclick="changeLng('en')" rel="" id="language-en-selector" class="text" key="en">English version</a>
            </span>
          </div>
        </div>
      </div>
      <div class="menu-bar clearfix">
        <div class="logo">
        </div>
        <a href="#eventsSection" rel="" class="text" key="start"></a>
        <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
        <a href="#partners" rel="" class="text" key="partners"></a>
        <a href="#newsletter" rel="" class="text" key="newsletter"></a>
        <a href="https://medium.com/tedxkrakow" target="_blank" rel="" class="text" key="blog"></a>
        <a href="#contact" rel="" class="text" key="contact"></a>
        <div id="mobile-menu" key="menu" class="mobile-menu text"></div>
        <span class="language-select">
          <a onclick="changeLng('pl')" rel="" id="language-pl-selector" class="text" key="pl">PL</a> | <a onclick="changeLng('en')" rel="" id="language-en-selector" class="text" key="en">EN</a>
        </span>
      </div>
    </div>

    <!-- END OF HEADER -->

    <!-- WELCOME SECTION -->
    <div id="start" class="section welcome">
      <div class="welcome-content">
        <div class="center-welcome-content clearfix">
          <div key="mainMessage" class="tedx text"></div>
          <div class="welcome-x-marks first-row clearfix">
            <div class="x-mark"></div>
            <div class="x-mark"></div>
            <div class="x-mark"></div>
            <div class="x-mark"></div>
          </div>
          <div class="welcome-x-marks second-row clearfix">
            <div class="x-mark"></div>
            <div class="x-mark"></div>
            <div class="x-mark"></div>
            <div class="x-mark"></div>
          </div>
        </div>
      </div>
      <div class="scroll-down-content">
        <a href="#eventsSection" rel="" >
          <div key="scrollDown" class="scroll-down text"></div>
          <svg class="scroll-down-arrow" version="1.1" id="Layer_1" xmlns="&ns_svg;" xmlns:xlink="&ns_xlink;" width="10" height="8"
            viewBox="0 0 10 8" overflow="visible" enable-background="new 0 0 10 8" xml:space="preserve">
            <polygon fill="#fff" points="10,0 5,8 0,0 "/>
          </svg>
        </a>
      </div>
    </div>
    <!-- END OF WELCOME SECTION -->


    <!-- EVENTS SECTION -->
    <div id="eventsSection" class="section events flex-grid">
      <div class="event-item event-previous col-2">
        <div class="item-content right-align">
          <div key="previousEventTitle" class="event-title text"></div>
          <div key="previousEventSubtitle" class="event-subtitle text"></div>
          <div key="previousEventDate" class="event-date text"></div>
          <div key="previousEventInfo" class="event-info text"></div>
        </div>
      </div>
      <div class="event-item col-2 event-upcoming">
        <div class="item-content left-align">
          <div key="nextEventTitle" class="event-title text"></div>
          <div key="nextEventSubtitle" class="event-subtitle text"></div>
          <div key="nextEventDate" class="event-date text"></div>
          <div key="nextEventInfo" class="event-info text"></div>
        </div>
      </div>
    </div>
    <!-- END OF EVENTS SECTION -->


    <!-- ABOUT US SECTION -->
    <div id="aboutUs" class="section three-items about-us">
      <div key="aboutUs" class="three-items-title text"></div>
      <div class="details">
        <div class="item">
          <div class="center-align three-items-content">
            <div key="aboutUsFirstTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsFirstInfo" class="item-text text"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="aboutUsSecondTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsSecondInfo" class="item-text text"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="aboutUsThirdTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsThirdInfo" class="item-text text"></div>
          </div>
        </div>
      </div>
      <div class="question text" key="questionOne"></div>
      <div class="answer">
        <a href="#eventsSection"><div id="yes" key="yes" class="active text"></div></a><a href="https://www.youtube.com/watch?v=d0NHOpeczUU" target="_blank"><div id="no" key="no" class="text"></div></a>
      </div>
    </div>
    <!-- END OF ABOUT US -->

    <!-- POSTS -->
    <div class="section posts">
      <div class="flex-grid post">
        <div class="col-2 post-photo post-photo-left" style="background-image:url(images/events/2017-live.jpg)">
          <img src="images/events/2017-live.jpg" />
        </div>
        <div class="col-2 post-info">
          <div class="item-content left-align">
            <div key="firstEventTitle" class="event-title text"></div>
            <div key="firstEventDate" class="event-subtitle text"></div>
            <div key="firstEventInfo" class="event-info text"></div>
          </div>
        </div>
      </div>
      <div class="flex-grid post reverse-column">
        <div class="col-2 post-info">
          <div class="item-content right-align">
            <div key="secondEventTitle" class="event-title text"></div>
            <div key="secondEventDate" class="event-subtitle text"></div>
            <div key="secondEventInfo" class="event-info text"></div>
          </div>
        </div>
        <div class="col-2 post-photo post-photo-right" style="background-image:url(images/events/2016-women.jpg)">
          <img src="images/events/2016-women.jpg" />
        </div>
      </div>
      <div class="flex-grid post">
        <div class="col-2 post-photo post-photo-left" style="background-image:url(images/events/2016-salon.jpg)">
          <img src="images/events/2016-salon.jpg" />
        </div>
        <div class="col-2 post-info">
          <div class="item-content left-align">
            <div key="thirdEventTitle" class="event-title text"></div>
            <div key="thirdEventDate" class="event-subtitle text"></div>
            <div key="thirdEventInfo" class="event-info text"></div>
          </div>
        </div>
      </div>
      <div class="flex-grid post reverse-column">
        <div class="col-2 post-info">
          <div class="item-content right-align">
            <div key="fourthEventTitle" class="event-title text"></div>
            <div key="fourthEventDate" class="event-subtitle text"></div>
            <div key="fourthEventInfo" class="event-info text"></div>
          </div>
        </div>
        <div class="col-2 post-photo post-photo-right" style="background-image:url(images/events/2015-dots.jpg)">
          <img src="images/events/2015-dots.jpg" />
        </div>
      </div>
    </div>
    <!-- END OF POSTS -->

    <!-- PARTNERS -->
    <div id="partners" class="section partners">
      <div class="content flex-grid">
        <div class="col-2">
          <div class="questions right-align item-content">
            <div id="partner-modal" key="partnerQuestionOne" class="question text"></div>
            <div key="partnerQuestionTwo" class="question text"></div>
            <div key="partnerQuestionThree" class="question text"></div>
          </div>
        </div>
        <div class="col-2">
          <form id="form" class="email-form left-align item-content">
            <div key="formName" class="email-label text"></div>
            <input id="name" class="email-input" type="text" name="name">
            <div key="formEmail" class="email-label text"></div>
            <input id="email" class="email-input" type="text" name="email">
            <div class="button-container">
              <button type="submit" key="send" class="text"></button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END OF PARTNERS -->

    <!-- PARNTERs MODAL -->
    <div class="partner-modal">
      <div class="backdrop">
      </div>
      <div class="modal">
        <div class="magnify">
          <div class="large"></div>
          <img class="small" src="images/partners-logos.jpg"/>
        </div>
      </div>
    </div>
    <!-- END OF PARTNERS MODAL -->

    <!-- CONTACT -->

    <div id="contact" class="section three-items">
      <div key="contact" class="three-items-title text"></div>
      <div class="details">
        <div class="item">
          <div class="center-align three-items-content">
            <div key="contactFirstTitle" class="item-title text"></div>
            <div key="contactFirstInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="contactSecondTitle" class="item-title text"></div>
            <div key="contactSecondInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="contactThirdTitle" class="item-title text"></div>
            <div key="contactThirdInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- END OF CONTACT -->

    <!-- FOOTER -->

    <div id="footer" class="section footer">
      <div class="footer-links">
        <div class="footer-site-links">
            <a href="#eventsSection" rel="" class="text" key="start"></a>
            <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
            <a href="#partners" rel="" class="text" key="partners"></a>
            <a href="#newsletter" rel="" class="text" key="newsletter"></a>
            <a href="#contact" rel="" class="text" key="contact"></a>
            <a href="#start" ret="" class="button-up" ></a>
        </div>
        <div class="footer-social-links">
            <!-- Facebook  -->
            <a target="_blank" href="https://facebook.com/tedxkrakow/">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <g fill="none" fill-rule="evenodd">
                      <path d="M0 0h24v24H0z"/>
                      <path fill="#FFF" d="M12.546 20H4.883A.883.883 0 0 1 4 19.117V4.883C4 4.395 4.395 4 4.883 4h14.234c.488 0 .883.395.883.883v14.234a.883.883 0 0 1-.883.883H15.04v-6.196h2.08l.31-2.415h-2.39V9.848c0-.7.194-1.176 1.196-1.176h1.28v-2.16c-.222-.03-.981-.095-1.864-.095-1.844 0-3.106 1.125-3.106 3.191v1.781h-2.085v2.415h2.085V20z"/>
                  </g>
              </svg>
            </a>
            <!-- Flickr  -->
            <a target="_blank" href="http://flickr.com/photos/tedxkrakow/">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <g fill="none" fill-rule="evenodd">
                      <path d="M0 0h24v24H0z"/>
                      <path fill="#FFF" fill-rule="nonzero" d="M23.786 12.166c0 2.853-2.342 5.166-5.238 5.166-2.897 0-5.242-2.313-5.242-5.166C13.306 9.314 15.651 7 18.548 7c2.896 0 5.238 2.314 5.238 5.166M10.478 12.166c0 2.853-2.344 5.166-5.242 5.166C2.343 17.332 0 15.02 0 12.166 0 9.314 2.343 7 5.236 7c2.898 0 5.242 2.314 5.242 5.166"/>
                  </g>
              </svg>
            </a>
            <!-- Insta  -->
            <a target="_blank" href="https://instagram.com/tedxkrakow/">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <g fill="none" fill-rule="evenodd">
                      <path d="M0 0h24v24H0z"/>
                      <path fill="#FFF" d="M12 4c2.173 0 2.445.01 3.298.048.852.039 1.433.174 1.942.372.526.204.973.478 1.417.923.445.444.719.89.923 1.417.198.509.333 1.09.372 1.942C19.99 9.555 20 9.827 20 12s-.01 2.445-.048 3.298c-.039.852-.174 1.433-.372 1.942a3.922 3.922 0 0 1-.923 1.417c-.444.445-.89.719-1.417.923-.509.198-1.09.333-1.942.372-.853.039-1.125.048-3.298.048s-2.445-.01-3.298-.048c-.852-.039-1.433-.174-1.942-.372a3.922 3.922 0 0 1-1.417-.923 3.921 3.921 0 0 1-.923-1.417c-.198-.509-.333-1.09-.372-1.942C4.01 14.445 4 14.173 4 12s.01-2.445.048-3.298c.039-.852.174-1.433.372-1.942.204-.526.478-.973.923-1.417.444-.445.89-.719 1.417-.923.509-.198 1.09-.333 1.942-.372C9.555 4.01 9.827 4 12 4zm0 1.441c-2.136 0-2.39.009-3.233.047-.78.036-1.203.166-1.485.276-.374.145-.64.318-.92.598-.28.28-.453.546-.598.92-.11.282-.24.705-.276 1.485-.038.844-.047 1.097-.047 3.233s.009 2.39.047 3.233c.036.78.166 1.203.276 1.485.145.374.318.64.598.92.28.28.546.453.92.598.282.11.705.24 1.485.276.844.038 1.097.047 3.233.047s2.39-.009 3.233-.047c.78-.036 1.203-.166 1.485-.276.374-.145.64-.318.92-.598.28-.28.453-.546.598-.92.11-.282.24-.705.276-1.485.038-.844.047-1.097.047-3.233s-.009-2.39-.047-3.233c-.036-.78-.166-1.203-.276-1.485a2.478 2.478 0 0 0-.598-.92 2.478 2.478 0 0 0-.92-.598c-.282-.11-.705-.24-1.485-.276-.844-.038-1.097-.047-3.233-.047zm0 2.45a4.108 4.108 0 1 1 0 8.217 4.108 4.108 0 0 1 0-8.216zm0 6.776a2.667 2.667 0 1 0 0-5.334 2.667 2.667 0 0 0 0 5.334zm5.23-6.937a.96.96 0 1 1-1.92 0 .96.96 0 0 1 1.92 0z"/>
                  </g>
              </svg>
            </a>
            <!-- Youtube  -->
            <a target="_blank" href="https://youtube.com/results?search_query=tedxkrakow">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <g fill="none" fill-rule="evenodd">
                    <path d="M0 0h24v24H0z"/>
                    <path fill="#FFF" d="M9.962 14.95V8.56l6.102 3.207-6.102 3.185zm13.4-7.499s-.22-1.567-.898-2.257c-.859-.907-1.821-.911-2.263-.964C17.041 4 12.3 4 12.3 4h-.01s-4.741 0-7.902.23c-.442.053-1.404.057-2.264.964-.677.69-.897 2.257-.897 2.257S1 9.293 1 11.133v1.726c0 1.841.226 3.682.226 3.682s.22 1.567.897 2.258c.86.906 1.989.878 2.491.972 1.807.175 7.68.229 7.68.229s4.746-.007 7.907-.237c.442-.054 1.404-.058 2.263-.964.677-.69.898-2.258.898-2.258s.226-1.84.226-3.682v-1.726c0-1.84-.226-3.682-.226-3.682z"/>
                </g>
            </svg>
            </a>
            <!-- Medium  -->
            <a target="_blank" href="https://medium.com/tedxkrakow/">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <g fill="none" fill-rule="evenodd">
                      <path d="M0 0h24v24H0z"/>
                      <path fill="#FFF" d="M20.99 6.935l-.011-.009-.007-.003-5.718-2.838a.64.64 0 0 0-.277-.064.656.656 0 0 0-.556.3L11.13 9.63l4.132 6.663 5.746-9.268a.069.069 0 0 0-.017-.09M9.316 8.421v6.044l5.413 2.687-5.413-8.73M15.542 17.555l4.456 2.211c.58.288 1.05.087 1.05-.45V8.676l-5.506 8.88M8.376 6.944L2.791 4.172a.645.645 0 0 0-.283-.073c-.245 0-.417.187-.417.503v11.967c0 .32.237.7.525.843l4.92 2.441a.805.805 0 0 0 .354.092c.306 0 .52-.234.52-.629V6.998a.06.06 0 0 0-.034-.054"/>
                  </g>
              </svg>
            </a>
        </div>
      </div>
      <div key="copyright" class="copyrights text"></div>
    </div>

    <!-- END OF FOOTER -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
    <script src="typed.min.js"></script>
    <script src="script.js" type="text/javascript"></script>
    <script src="messages.js" type="text/javascript"></script>
  </body>
</html>
