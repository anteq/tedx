var messages = {
  en: {
    mainMessage: "Hello, are you a TEDxer?",
    aboutUs: "About us",
    start: "Start",
    partners: "Partners",
    newsletter: "Newsletter",
    contact: "Contact",
    blog: "Blog",

    menu: "Menu",

    scrollDown: "Scroll down",

    previousEventTitle: "Previous event",
    previousEventSubtitle: "TEDxKraków Adventure: Look at the city",
    previousEventDate: "22th June 2017<br />Hala Główna",
    previousEventInfo: "3 speakers from local grassroots organizations take us on a journey through the city as they see it, followed by workshops broken up into various stages of the design thinking methodology applied to placemaking.",

    nextEventTitle: "Next event",
    nextEventSubtitle: "TEDxKraków 2017: Turning Points",
    nextEventDate: "28th October 2017<br />?",
    nextEventInfo: "Save the date!",

    aboutUsFirstTitle: "Inspire",
    aboutUsFirstInfo: "Kraków deserves to be on the world innovation map because of its people and its ideas. Our events: big conferences and small meet ups amplify the most thought-provoking ideas and resonate back engaging local community.",

    aboutUsSecondTitle: "Connect",
    aboutUsSecondInfo: "Almost 10k people got inspired at our events so far. The beat of thrilling ideas and cutting-edge innovation was pounding blatantly. We’re a group of volunteers, yet we approach TEDxKraków as professionals. It’s our baby and there’s more to come.",

    aboutUsThirdTitle: "Change",
    aboutUsThirdInfo: "We enable local ideas to be heard about, we bring global ideas to our local community. It’s all sizzling. Our events are not only about listening, we empower our participants to bring the change. We’re happy to take you down the road through the TEDxKraków alumni alley. You will be impressed.",

    questionOne: "Are you familiar with TED?",
    yes: "yes",
    no: "no",

    firstEventYear: '2017',
    firstEventTitle: 'TEDxKrakówLive: The Future You',
    firstEventDate: '28 April 2017<br />Cheder',
    firstEventInfo: 'A live streaming of our favorite session from TED\'s 2017 conference: <a href="https://ted2017.ted.com/">The Future You</a>, warmed up with an origami paper crane workshop.',

    secondEventYear: '2016',
    secondEventTitle: 'TEDxKrakówWomen: It\'s About Time',
    secondEventDate: '28 October 2016<br />Spotkawie',
    secondEventInfo: 'An evening dedicated to women, enjoyed by all. Four live speakers mixed in with a selected session from the <a href="https://tedwomen2016.ted.com/">TEDWomen 2016 conference in San Francisco</a>. Read more about our <a href="https://medium.com/tedxkrakow/tedxkrak%C3%B3wwomen-its-about-time-b2e6eb0ad4ec">event</a>, our complementary <a href="https://medium.com/tedxkrakow/%C5%BCeby-da%C4%87-wam-szans%C4%99-do%C5%9Bwiadczenia-g%C5%82%C4%99biej-magii-tedxkrak%C3%B3wwomen-we-wsp%C3%B3%C5%82pracy-ze-wspania%C5%82ymi-74f1837149c">workshops</a> and our specially curated <a href="https://medium.com/tedxkrakow/zapraszamy-na-tedxkrakowwomen-alley-ae726067be3c">NGO Valley</a>. ',

    thirdEventYear: '2016',
    thirdEventTitle: '8 x TEDxKrakówSalon',
    thirdEventDate: 'January - November 2016',
    thirdEventInfo: 'In 2016 we set out on a mission: 8 salon events, focused around 8 open-ended themes, in places scattered across our city that make us think about those themes in a different way. Our salons were each focused on one concept, brought to life via a selection of TED and TEDx Talks, and were followed by stimulating discussion. <a href="https://medium.com/tedxkrakow/what-s-a-tedxkrakowsalon-920325b7d84d">Read more</a> about what to expect at a salon event and check our our ambitious mission.',

    fourthEventYear: '2015',
    fourthEventTitle: 'TEDxKraków 2015: Reconnect the dots',
    fourthEventDate: '18 October 2015<br />ICE',
    fourthEventInfo: 'In an event culminating pretty much 5 years of TEDxKrakow\'s to-date history, we brought together over 1,100 locals and travellers from distant places, popped their minds open, and let the inspiration and ideas worth spreading fill in. See the <a href="http://tedxkrakow.com/archive/pl/events/66-TEDxKrakow2015.html">live blog</a> and <a href="https://www.flickr.com/photos/tedxkrakow/albums/72157654087722419">photos</a>.',

    partnerQuestionOne: "Do you want to be our <span onclick='showPartnersModal()'>partner</span>?",
    partnerQuestionTwo: "Or, do you want to join our <span onclick='showTeamModal()'>team</span>?",
    partnerQuestionThree: "Maybe, you want to subscribe to ‘Cześć’ from us?",

    formName: "Name",
    formEmail: "Email",
    send: "Send",

    contact: "Contact",

    contactFirstTitle: "GENERAL CONTACT",
    contactFirstInfo: "contact@tedxkrakow.com",
    contactSecondTitle: "Press & Media",
    contactSecondInfo: "media@tedxkrakow.com",
    contactThirdTitle: "PARTNERSHIP",
    contactThirdInfo: "partnership@tedxkrakow.com",
    copyright: "<strong>TEDxKraków, rocking since 2010. What we have done? See our <a style='color:white' href='http://tedxkrakow.com/archive'>archive</a>.</strong><br />© TEDxKraków - All rights reserved<br /><br />If you want to improve this website with us, feel free to contact <a href=\"mailto:domi@tedxkrakow.com\">designer</a> or <a href=\"mailto:antek@tedxkrakow.com\">developer</a>.<br />Special thanks to <a href=\"mailto:m4kasprzak@gmail.com\">Marek</a> who helped with development."

  },
  pl: {
    mainMessage: "Cześć, jesteś TEDxerem?",
    aboutUs: "O nas",
    start: "Start",
    partners: "Partnerzy",
    newsletter: "Newsletter",
    contact: "Kontakt",
    blog: "Blog",

    menu: "Menu",

    scrollDown: "Zobacz więcej",

    previousEventTitle: "Poprzedni event",
    previousEventSubtitle: "TEDxKraków Adventure: Spójrz na miasto",
    previousEventDate: "22 czerwca 2017<br />Hala Główna",
    previousEventInfo: "Miasto pod lupą 3 prelegentów z lokalnych organizacji oraz 3 warsztaty prowadzone równolegle metodą design thinking.",

    nextEventTitle: "Następny event",
    nextEventSubtitle: "TEDxKraków 2017: Punkt Zwrotny",
    nextEventDate: "28 pażdziernika 2017<br />?",
    nextEventInfo: "Zapisz w kalendarzu!",

    aboutUsFirstTitle: "Inspirujemy",
    aboutUsFirstInfo: "Wierzymy, że Kraków powinien zająć szczególną pozycję na mapie innowacyjnych ośrodków dzięki ludziom i ich ideom. Nasze wydarzenia - duże konferencje oraz kameralne spotkania, służą rozpowszechnianiu idei wartych szerzenia i angażują lokalną społeczność do działania.",

    aboutUsSecondTitle: "Łączymy",
    aboutUsSecondInfo: "Już prawie 10 tys. osób pojawiło się na wydarzeniach TEDxKraków aby zaczerpnąć inspiracji. Porywające idee i przełamujące schematy pomysły nie raz powodowały u publiczności szybsze bicie serca. Jako zespół w każde wydarzenie angażujemy się z pełną mocą. Mimo, że jesteśmy grupą wolontariuszy, kiedy mowa o TEDxKraków zamieniamy się w profesjonalistów!",

    aboutUsThirdTitle: "Zmieniamy",
    aboutUsThirdInfo: "Zapraszamy na scenę lokalnych działaczy, aby usłyszał o nich świat. Jednocześnie prezentujemy globalne idee, aby znalazły możliwość realizacji w krakowskiej społeczności. Uczestników naszych wydarzeń zachęcamy, by nie tylko słuchali, ale też sami katalizowali zmiany. Zajrzyj do naszego archiwum, będziesz pod wrażeniem!",

    questionOne: "Czy znasz TED?",
    yes: "Tak",
    no: "Nie",

    firstEventYear: '2017',
    firstEventTitle: 'TEDxKrakówLive: The Future You',
    firstEventDate: '28 kwietnia 2017<br />Cheder',
    firstEventInfo: 'Live Streaming wprost ze sceny w Vancouver wybranej przez nas sesji wystąpień \'The Future You\' oraz warsztaty wykonywania żurawi techniką origami. ',

    secondEventYear: '2016',
    secondEventTitle: 'TEDxKrakówWomen: It\'s About Time',
    secondEventDate: '28 października 2016<br />Spotkawie',
    secondEventInfo: 'Wieczór dedykowany kobietom. 4 prelegentki na żywo na scenie, streaming z konferencji <a href="https://tedwomen2016.ted.com/">TEDWomen w San Francisco</a>, unikalne warsztaty oraz NGO Valley.',

    thirdEventYear: '2016',
    thirdEventTitle: '8 x TEDxKrakówSalon',
    thirdEventDate: 'Styczeń - Listopad 2016',
    thirdEventInfo: '8 spotkań w formule salonu w 8 różnych miejscach na mapie Krakowa, 8 tematów przewodnich i niezliczona ilość dyskusji między uczestnikami. Przeczytaj więcej o Salonach <a href="https://medium.com/tedxkrakow">tutaj</a>.',

    fourthEventYear: '2015',
    fourthEventTitle: 'TEDxKraków 2015: Reconnect the dots',
    fourthEventDate: '18 października 2015<br />ICE',
    fourthEventInfo: 'Największe wydarzenie zorganizowane dotychczas: 1100 uczestników, kilkanaścioro prelegentów/-tek występujących na żywo, występy artystyczne, ogrom inspiracji.',

    partnerQuestionOne: "Czy chcesz zostać naszym <span onclick='showPartnersModal()'>partnerem</span>?",
    partnerQuestionTwo: "A może zastanawiasz się nad dołączeniem do <span onclick='showTeamModal()'>zespołu</span>?",
    partnerQuestionThree: "Albo chcesz otrzymywać 'Cześć' od czasu do czasu?",

    formName: "Imię",
    formEmail: "Mail",
    send: "Wyślij",

    contact: "Kontakt",

    contactFirstTitle: "Kontakt ogólny",
    contactFirstInfo: "contact@tedxkrakow.com",
    contactSecondTitle: "Media i marketing",
    contactSecondInfo: "media@tedxkrakow.com",
    contactThirdTitle: "Partnerzy",
    contactThirdInfo: "partnership@tedxkrakow.com",

    copyright: "<strong>TEDxKraków, działamy od 2010. Czego dokonaliśmy do tej pory? Sprawdź nasze <a style='color:white' href='http://tedxkrakow.com/archive'>archiwum</a>.</strong><br />© TEDxKraków - Wszystkie prawa zastrzeżone<br /><br />Jeżeli chcesz udosknolić tą stronę z nami, skontaktuj się z  <a href=\"mailto:domi@tedxkrakow.com\">projektantem</a> lub <a href=\"mailto:antek@tedxkrakow.com\">programistą</a>.<br />Specjalne podziękowanie dla <a href=\"mailto:m4kasprzak@gmail.com\">Marka</a>, ktory pomógł w realizacji tej strony."
  }
}

var lng;

function changeLng(l) {
  if (l !== lng) {
    lng = l;
    $('.language-select a').removeClass('active');
    $("#language-" + l + "-selector").addClass('active');
    $(".text").each(function(){
      $(this).html(messages[lng][$(this).attr("key")])
    })
    runMainText();
    runXAnimation()
  }
}

changeLng('en');

$('#language, #mobile-language').change(function(event) {
  $('#language, #mobile-language').val(event.target.value)
  changeLng(event.target.value);
});
