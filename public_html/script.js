
// script for scrolling
$('a').click(function(){
    if ($(this).attr('href') && $(this).attr('href').indexOf('http') !== 0) {
      var offset = $('#header').css('position') == 'absolute' ? 0 : 80
      $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top - offset
      }, 500);
      return false;
    }
});

function sendMail(email, name) {
  $.ajax( { type : 'POST',
    data : {email: email, name: name},
    url  : 'mailSender.php',
  });
}

function showPartnersModal() {
  $('.partner-modal').addClass("show-modal")
}

$('#mobile-menu').click(function(){
  if ($('#header').hasClass("fix-header")) $('#header').removeClass("fix-header")
  if ($('#header').hasClass("extend-header")){
    $('#header').removeClass("extend-header")
  } else {
    $('#header').addClass("extend-header")
  }
});

$('.close-menu').click(function(){
  $('#mobile-header').removeClass("show-header")
});

$('.backdrop').click(function(){
  $('.partner-modal').removeClass("show-modal")
});


$('#form').submit(function( event ) {
  event.preventDefault();
  var email = $('#form #email');
  var name = $('#form #name');
  sendMail(email.val(), name.val())
  name.val('')
  email.val('')
});

var doc = $(document);
doc.on("scroll", function(e) {
  if ($('#header').css('position') == 'absolute') return
  if (doc.scrollTop() > 0) {
    $('#header').addClass("fix-header");
  } else {
    $('#header').removeClass("fix-header");
  }
});

var typed;

var runMainText = function(){
  // if (typed) { typed.stop(); $('#mainClaim').empty(); }
  // setTimeout(function(){
  //   console.debug('wtf');

  //   typed = undefined;
  //   setTimeout(function(){
  //     typed = new Typed('#mainClaim', {
  //       strings: [messages[lng]['mainMessage']],
  //       typeSpeed: 100,
  //       onComplete: function(self) {
  //         $('.scroll-down-content').css('opacity', 1);
  //       }
  //     });
  //   }, 500)
  // });
};

function runXAnimation() {
  $('.welcome-x-marks .x-mark').css('animation', 'none')
  setTimeout(() => {
    $('.welcome-x-marks .x-mark').css('animation', '').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function(){
      $('.scroll-down-content').css('opacity', 1);
    })
  }, 0)
}


$(document).ready(function(){

	var native_width = 0;
	var native_height = 0;
  $(".large").css("background","url('" + $(".small").attr("src") + "') no-repeat");

	//Now the mousemove function
	$(".magnify").mousemove(function(e){
		//When the user hovers on the image, the script will first calculate
		//the native dimensions if they don't exist. Only after the native dimensions
		//are available, the script will show the zoomed version.
		if(!native_width && !native_height)
		{
			//This will create a new image object with the same image as that in .small
			//We cannot directly get the dimensions from .small because of the
			//width specified to 200px in the html. To get the actual dimensions we have
			//created this image object.
			var image_object = new Image();
			image_object.src = $(".small").attr("src");

			//This code is wrapped in the .load function which is important.
			//width and height of the object would return 0 if accessed before
			//the image gets loaded.
			native_width = image_object.width;
			native_height = image_object.height;
		}
		else
		{
			//x/y coordinates of the mouse
			//This is the position of .magnify with respect to the document.
			var magnify_offset = $(this).offset();
			//We will deduct the positions of .magnify from the mouse positions with
			//respect to the document to get the mouse positions with respect to the
			//container(.magnify)
			var mx = e.pageX - magnify_offset.left;
			var my = e.pageY - magnify_offset.top;

			//Finally the code to fade out the glass if the mouse is outside the container
			if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
			{
				$(".large").fadeIn(100);
			}
			else
			{
				$(".large").fadeOut(100);
			}
			if($(".large").is(":visible"))
			{
				//The background position of .large will be changed according to the position
				//of the mouse over the .small image. So we will get the ratio of the pixel
				//under the mouse pointer with respect to the image and use that to position the
				//large image inside the magnifying glass
				var rx = Math.round(mx/$(".small").width()*native_width - $(".large").width()/2)*-1;
				var ry = Math.round(my/$(".small").height()*native_height - $(".large").height()/2)*-1;
				var bgp = rx + "px " + ry + "px";

				//Time to move the magnifying glass with the mouse
				var px = mx - $(".large").width()/2;
				var py = my - $(".large").height()/2;
				//Now the glass moves with the mouse
				//The logic is to deduct half of the glass's width and height from the
				//mouse coordinates to place it with its center at the mouse coordinates

				//If you hover on the image now, you should see the magnifying glass in action
				$(".large").css({left: px, top: py, backgroundPosition: bgp});
			}
		}
	})
})
